import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PythontestService {

  constructor(private _http: HttpClient) { }


  public pingService() : Observable<object> {
    console.log("GET SERVICE") ;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this._http.get("https://mytenant.mindsphere.io/hello/world",httpOptions)
      .pipe(map((response: Response) => <object> response.json()));
   } 


  /* public pingPostService() : Observable<object> {
    console.log("POST SERVICE!!") ;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        "Accept": "application/json",
        "x-xsrf-token": this.getCookie("XSRF-TOKEN"),
        "origin": `${window.location.protocol}//${window.location.host}`
      })
    };

    let body={}

    console.log("headers:"+httpOptions)
    return this._http.post("https://https://mytenant.mindsphere.io/v1/ping",body,httpOptions)
      .pipe(map((response: Response) => <object> response.json()));
   } */

   public getCookie(name){
    if (!document.cookie) {
      return null;
    }
    const cookies = document.cookie.split(';').map(c => c.trim())
        .filter(c => c.startsWith(name + '='));
    if (cookies.length === 0) {
      return null;
    }
    return decodeURIComponent(cookies[0].split('=')[1]);
   }
   
}
