import { TestBed } from '@angular/core/testing';

import { PythontestService } from './pythontest.service';

describe('PythontestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PythontestService = TestBed.get(PythontestService);
    expect(service).toBeTruthy();
  });
});
