import { Component } from '@angular/core';
import { PythontestService } from './pythontest.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'boq-user-interface-test';

  constructor(private _pythonService : PythontestService) { }

  ngOnInit() {
    this.pingService()
  }

  public pingService(){
    this._pythonService.pingService()
    .subscribe((lstresult : object) =>{
      console.log("API call is success:"+lstresult)
    },
    error=>{
      console.log("Error.Ping service");
      console.log(error);
    }
    );

    this._pythonService.pingPostService()
    .subscribe((lstresult : object) =>{
      console.log("API call is success:"+lstresult)
    },
    error=>{
      console.log("Error.Ping service");
      console.log(error);
    }
    );
  }
}
